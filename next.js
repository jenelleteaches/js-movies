
let id = localStorage.getItem("movieId")
if (id === null || id === undefined) {
  console.log("No movie id defined.")
  $("results").html("Sorry, no movie found")
}
else {
  const URL = `https://api.themoviedb.org/3/movie/${id}/videos?api_key=041b7ff5799fd5e107ddb2e5d1211552&language=en-US`

  console.log("connecting to: " + URL);


  $.ajax({
    dataType:"json",
    url:URL,
    success:function(responseData, status, xhr) {
      // 1. Get response from website and automatically
      // convert it to DICTIONARY or ARRAY
      console.log(responseData)

      // 2. put your parsing nonsense here
      let trailers = responseData["results"]

      if (trailers.length == 0) {
        console.log("This movie does not have any trailers.")
        $("results").html("Sorry, this movie has no trailers.")
        return
      }

      let trailer = trailers[0]

      // 3. get the video key
      let videoId = trailer["key"]

      // 4. create a youtube link
      let url = `https://www.youtube.com/embed/${videoId}?autoplay=1`

      // 5. generate html to embed youtube video in website
      let html = `<iframe width="560" height="315" src="${url}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`

      // 6. put the html in the front end
      $("#results").append(html)

    },      // end success function
    error: function(jqXhr, textStatus, errorMesage) {
      alert("error");
      console.log('Error: ' + errorMessage); // An error occurred during the request.
    }
  })    // end ajax call


} // end else statement
