function search()  {

    // clear the results div
    $("#results").empty();


    const URL = "https://api.themoviedb.org/3/movie/now_playing?api_key=041b7ff5799fd5e107ddb2e5d1211552&language=en-US&page=1"
    console.log("connecting to: " + URL);

    $.ajax({
      dataType:"json",
      url:URL,
      success:function(responseData, status, xhr) {
        alert("Success!")
        console.log(responseData)

        let movies = responseData["results"]    // 1. parse out the "results" data from the api
        for (let i = 0; i < movies.length; i++) {

          // 3. get the individual movie
          let movie = movies[i]

          // 4. parse out what you need
          let id = movie["id"]
          let name = movie["title"]
          let imagePath = movie["poster_path"]
          let summary = movie["overview"]
          let date = movie["release_date"]

          // 5. output to console to test that you got all the correct data
          console.log("Movie name: " + name)
          console.log("Poster: " + imagePath)
          console.log("Summary: " + summary)
          console.log("Release Date: " + date)


          console.log("--------")

          // 6. build the image link to the poster
          let imageUrl = `https://image.tmdb.org/t/p/w185${imagePath}`

          // 7 . build the html
          let html = ""
          html += "<div class='card'>"
          html += `<h2>${name}</h2><br>`
          html += `<img class="poster" src="${imageUrl}"><br>`
          html += `<h3>${summary}</h3> <br>`
          html += `Release Date: ${date} <br>`
          html += "</p>"
          html += "<p>"
          html += `<a href="#" onClick="addToFavorites('${name}')" class="favoriteLink"><i class="far fa-heart"></i></a>`
          html += `<a href="#" onClick="showTrailer('${id}')">Watch Trailer</a>`
          html += "</p>"
          html += "</div>"

          // 8. put the html in the front end
          $("#results").append(html)


        } // end for loop
      },      // end success function
      error: function(jqXhr, textStatus, errorMesage) {
        alert("error");
        console.log('Error: ' + errorMessage); // An error occurred during the request.
      }
    })    // end ajax call

} // end search function

function addToFavorites(name) {
  // debug: output whatever person clicked on
  console.log(name);

  let favorites = localStorage.getItem("favoritesList")
  if (favorites === undefined || favorites === null) {
    // no previously saved movies, therefore initialize as an empty array
    console.log("null value")
    favorites = []
  }
  else {
    // i found some movies, so cast the string to an array
    console.log("found something")
    favorites = JSON.parse(favorites)
  }

  // add movie to array
  favorites.push(name);

  // convert movies list back to a string and store it in localStorage
  localStorage.setItem("favoritesList", JSON.stringify(favorites))

  console.log("saved to local storage")
  alert("Saved to favorites!")
}

function showFavorites() {
  console.log("show favorites list pushed")


  let html = ""

  // get items from localstorage
  let favorites = localStorage.getItem("favoritesList")
  if (favorites === undefined || favorites === null) {
    // no previously saved movies, therefore initialize as an empty array
    console.log("no movies in favorites list")
    html += "No movies in your favorites list."
  }
  else {
    // get the movies and convert back to array
    favorites = JSON.parse(favorites)
    for (let i = 0; i < favorites.length; i++) {
      html += `${favorites[i]} <br>`
    }
  }

  // put results in front end
  $("#results").html(html)

}

function showTrailer(id) {
  localStorage.setItem("movieId", id);
  window.location.href ="next.html"
}
